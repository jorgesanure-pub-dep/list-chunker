import 'package:list_chunker/list_chunker.dart';

void main() => print(List.generate(10, (i) => i).chunk(3));

// Result
// [[0, 1, 2], [3, 4, 5], [6, 7, 8], [9]]
