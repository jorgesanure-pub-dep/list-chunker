extension ListChunkerExtension on List {
  List chunk(int chunkLength) {
    if ((chunkLength ?? 0) <= 0 || (this ?? []).length == 0) return this;
    var chunks = [];
    for (var i = 0; i < this.length; i += chunkLength) {
      chunks.add(this.sublist(
          i, i + chunkLength > this.length ? this.length : i + chunkLength));
    }
    return chunks;
  }
}

class ListChunker {
  ListChunker._();
  static List chunk(List list, chunkLength) => list.chunk(chunkLength);
}
