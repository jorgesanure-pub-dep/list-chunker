# list_chunker

Extension to chunk list

## DEMO
[Dartpad](https://dartpad.dartlang.org/968ebf2e3e957c2d074efdf660efa200?)

```dart
List.generate(10, (i) => i).chunk(3)

// [[0, 1, 2], [3, 4, 5], [6, 7, 8], [9]]
```